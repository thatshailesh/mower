const directions = ["N", "E", "S", "W"];

const dirLength = directions.length;

class Mower {
  constructor(coordinates = [], direction = null, lawn = []) {
    if (!coordinates.length || !direction || !lawn.length) {
      throw new Error("Unmet dependencies");
    }
    this.x = coordinates[0];
    this.y = coordinates[1];
    this.direction = direction;
    this.xBoundary = lawn[0];
    this.yBoundary = lawn[1];
  }

  isValidMove(x, y) {
    return x >= 0 && x <= this.xBoundary && y >= 0 && y <= this.yBoundary;
  }

  get position() {
    return `${this.x} ${this.y} ${this.direction}`;
  }

  rotate(command) {
    const index = directions.indexOf(this.direction);
    // console.log("index ->> ", index);
    if (command === "left")
      return index > 0 ? directions[index - 1] : directions[dirLength - 1];

    if (command === "right") {
      return index === dirLength - 1 ? directions[0] : directions[index + 1];
    }
  }

  turnLeft() {
    this.direction = this.rotate("left");
    // console.log("After left - > ", this.direction);
  }

  turnRight() {
    this.direction = this.rotate("right");
    // console.log("After right - > ", this.direction);
  }

  move() {
    switch (this.direction) {
      case "E":
        {
          if (this.isValidMove(this.x + 1, this.y)) {
            this.x += 1;
          }
        }
        break;
      case "W":
        {
          if (this.isValidMove(this.x - 1, this.y)) {
            this.x -= 1;
          }
        }
        break;
      case "N":
        {
          if (this.isValidMove(this.x, this.y + 1)) {
            this.y += 1;
          }
        }
        break;
      case "S":
        {
          if (this.isValidMove(this.x, this.y - 1)) {
            this.y -= 1;
          }
        }
        break;
    }
  }
}

module.exports = Mower;
