const { expect } = require("chai");
const Mower = require("../lib/mower");
describe("Lawn Mower", () => {
  let mower = new Mower([1, 1], "N", [5, 5]);

  it("should turn right", function() {
    mower.turnRight();
    expect(mower.direction).to.eql("E");
  });

  it("should turn left", function() {
    mower.turnLeft();
    expect(mower.direction).to.eql("N");
  });

  it("should move up", function() {
    mower.move();
    expect(mower.y).to.eql(2);
  });
  it("should move down", function() {
    mower.move();
    expect(mower.y).to.eql(3);
  });
  it("should not move beyond boundary", function() {
    mower = new Mower([1, 1], "N", [1, 1]);
    mower.move();
    expect(mower.y).to.eql(1);
  });
});
