## **Lawn Mower**

An Automatic lawn mower designed to mow rectangular surfaces.
(The mower can be programmed to mow the entire surface.)

## Getting Started

`npm i`

## Tests

`npm run test`

